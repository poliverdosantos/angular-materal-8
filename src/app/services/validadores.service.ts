import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidadoresService {

  date;

  constructor() {
    this.date = new Date().getFullYear();
   }
   
}
