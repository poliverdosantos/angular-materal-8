import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ValidadoresService } from 'src/app/services/validadores.service';


@Component({
  selector: 'app-tarjetas',
  templateUrl: './tarjetas.component.html',
  styleUrls: ['./tarjetas.component.css']
})
export class TarjetasComponent implements OnInit {

  
  form!: FormGroup;

  get edadNoValid(){
    return this.form.get('edad')?.invalid && this.form.get('edad')?.touched;
  }

  constructor() {
}
  ngOnInit(): void {
  }

}
